.. GraviDy: Gravitational Dynamics documentation master file, created by
   sphinx-quickstart on Tue May 20 13:54:58 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GraviDy: Gravitational Dynamics
================================

The direct-summation of *N-* gravitational forces is a complex problem for which
there is not analytical solution.
Dense stellar systems such as galactic nuclei and stellar
clusters but also protoplanetary discs are the loci of different interesting
and different problems, ranging from the global evolution of globular clusters,
tidal disruptions of stars by a massive black hole, the formation of
protoplanets or the potential detection of sources of gravitational radiation,
to mention a few.

**GraviDy** is a new GPU, direct-summation N-body integrator written
from scratch and based on the Hermite scheme.
The most important features of the code are:

 * Written in C/C++,
 * Using parallel computing techniques and libraries like OpenMP, MPI and CUDA,
 * full double-precision resolution,
 * its high modularity, which allows users to readily introduce new
   physics into it,
 * the exploitation of all high-performance computing
   resources available,
 * its maintenance and improvement cycle,
 * the fact that the code is publicly released under a BSD license and will be
   maintained via planned, public, regular updates.


Getting the code
-----------------

 * From `Gitlab`_

   .. code-block:: bash

       git clone http://gitlab.com/cmaureir/gravidy.git

 * From `Github`_

   .. code-block:: bash

       git clone http://github.com/cmaureir/gravidy.git


 * Alternatively you can download the lastest release as a `tar.gz from here`_.

.. _`Gitlab`: http://gitlab.com/cmaureir/gravidy
.. _`Github`: http://github.com/cmaureir/gravidy
.. _`tar.gz from here`: http://gravidy.xyz/gravidy-master.tar.gz

Authors
------------

**Cristián Maureira-Fredes**
 * http://maureira.xyz
 * cristian.maureira.fredes [at] aei.mpg.de

**Pau Amaro-Seoane**
 * http://astro-gr.org
 * pau [at] ice.cat

Publication
------------

The publication about this project can be found `here`_.

.. _`here`: https://arxiv.org/abs/1702.00440

Documentation
---------------

Contents:

.. toctree::
   :maxdepth: 3

   include/setup.rst
   include/introduction.rst
   include/why-programming-matter.rst
   include/use.rst
   include/integrator.rst
   include/parallel-computing.rst
   include/gravidy-view.rst
   include/doxygen.rst
..   include/close-encounters.rst
.. include/preliminary-results.rst




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

