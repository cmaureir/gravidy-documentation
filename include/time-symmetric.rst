Hermite 4th order Time-symmetric
==================================

This integrator was presented in [Kokubo98]_ aiming for a planetary N-body
simulation without secular energy errors of periodic orbits.

The Hermite 4th order integration scheme (Predictor/Corrector)
depends of three main steps:

 1. Prediction of the particles (Extrapolation), P.
 2. Forces evaluation, E.
 3. Correction of the particles (Interpolation), C.

which can be abbreviated as `PEC`.

On the paper, a `P(EC)^n` algorithm is presented obtaining that `n=3` is a fair
value for getting a proper planetary integrator without secular energy errors.

Thus, the procedure is the same as the :ref:`hermite` section,
repeating the `EC` procedure three times, but with a couple modifications:

 * The first Force evaluation uses the predicted information,
   but the following evaluations uses the corrected one.
 * The correction step always use the same predicted information,
   the changing terms are the acceleration derivatives, which are interpolated
   from the force evaluation using the corrected positions and velocities.
 * At the end of the `EC` loop, we assign the last corrected position
   and velocity to the current values, to perform the prediction in the next
   step.
 * Since the interpolation process inside the correction step uses *old forces*
   this are not overwritten inside the loop, they are saved before entering the
   loop.
 * Due to the high-accuracy that the orbits must need, we don't have a lower
   limit for the time-step calculation.

A pseudo-code to explain this differences is the following

.. code-block:: python
    :linenos:

    r, v = initial_positions_velocities()
    f = initial_forces(r, v)

    while integration_time < T:
        r_p, v_p = prediction(r, v, f)
        old_f = save_old_forces(f)
        n = 0
        while n < 3:
            if n == 0:
                f = evaluation(r_p, v_p)
            else:
                f = evaluation(r_c, v_c)

            r_c, v_c = correction(f, old_f, r_p, v_p)
            n += 1

        r, v = r_c, v_c
        integration_time += 1

Time steps
-----------

The binary (multiple) integration procedure
considering the current time is :math:`t_{c}`,
the time at the end of the general integration step is :math:`t_{f} = t_{c} + \Delta t_{min}`
(Where :math:`\Delta t_{min}` is the minimum time step of the general N-body system),
can be represented as:

.. Mechanism of going forward and backwards on time -- still pending
.. Add Kaplan paper


.. [Kokubo98] On a time-symmetric Hermite integrator for planetaryN-body simulation
    http://adsabs.harvard.edu/abs/1998MNRAS.297.1067K

