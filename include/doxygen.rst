Doxygen documentation
======================

The auto generated *in-line* code documentation
can be found `here`_.

.. _`here`: ../doxygen/
