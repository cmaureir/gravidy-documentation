Why Programming Matter
=======================

There are several *N-* body integrators available online,
which provide a lot of features, including a good
handling of physical phenomena, like close encounters,
regularisation, etc, from which if you use it as a
**black-box** will be the perfect solution for a simple
integration scheme, but the problems appear when you
need to modify certain functionality or process
inside the code, from which you need either experience
working inside that determined code or time analysing
where there is the piece of procedures that you want to modify.

This task if often a bottleneck, since users which do not have
programming knowledge, or simply not much experience will
encounter a few issues, that can generate misleading results.

A few of them, provide a couple of *in-line* documentation,
comments, publications references, which is not enough.
On the other hand, the documentation mechanism are not appropriate
for documenting code.

A couple of *not-so-good* practices related to the software itself are:

 * Documents including all the acronyms of the code, since variables
   and constant must be self-explanatory.
 * Different comments in different versions of the code, generating uncertainty
   among the functionalities in different versions.
 * Not having up-to-date source in an official website, and distribute the
   development version via email or unofficial ways.
 * Not having a collaboration platform for the development branch
   (As a distributed revision control)
 * Missing unit testing for critical sections/functions of the code.

Language
---------

Why C++?

Most of the scientific programs are written in Fortran,
due to his popularity among programmers and non-programmers in the last decades,
but even if the code is slightly faster than other compiled languages,
is a programming language which is not evolving fast like all the
modern languages, this means that the integration with newer tools and libraries
is not *easy-to-go*.

The use of modern languages, like Python, Go, among others
was discarded since the performance for a :math:`O(N^2)` will not be
very good, and the need for a C/C++/Fortran wrapper for the
heavily-computational functions will be necessary,
or even the use of a external modules to speed up the code.

This is the main reason of using C/C++,
since is a good intermediate point between Fortran
and modern languages.
Additionally, C++ provides an Object-Oriented (OO) programming scheme,
which simplify the interaction of the elements inside the code, and allow us
to have more modularity.

Object orientation
-------------------

Mainly, the decision of using OO programming consist
in the following points:

 * **Encapsulation**, to pack a set of functions and data into the component,
   like for example a determined integration scheme.
 * **Class inheritance**, to allow the implementation of different optimisation
   techniques inheriting the same base-scheme.
 * **Modularity**, for maintainability and be able to expand the implementation.

Variable naming
----------------

All the constant inside the code are written in capital letters,
to note them inside any function.

Dealing with GPU programming needs a proper naming convention,
since often is necessary to have the same array data in the Host
and the GPU-Device.
We use the prefix ``h_`` for host arrays and ``d_`` for device arrays.

The classes naming follows the CamelCase rule.

Documentation
---------------

Every function contains a piece of comment before the declaration
following the Doxygen format, to automatically generate a documentation
static website.

Additionally, we include a few *in-line* comments for function calling
that may not be clear, and for the main steps inside the integration.
