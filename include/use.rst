How to use it
--------------

Command line options
=====================

The Available options for using ``GraviDy`` from the command line are the following:

* ``-h`` Print the help message, including all the available options.

* Mandatory
    * ``-i`` Option to specify the initial conditions to be used.
    * ``-t`` Integration time in N-body units, if this is not specified, the default
      value is `1`.
* Optional
    * ``-o`` Specify the name of the output files that will be used to generate
      all the simulations logs.
    * ``-s`` The default value of the softening `\epsilon` is `1e-4` but can be
      changed using this option.
    * ``-e`` The default value of `\eta` is `0.01`, but can be changed using this
      option.
    * ``-p`` Instead of writing a file(s) with all the simulation logs, this can
      be printed on the screen (stdout).
    * ``-z`` Time interval to get the output from the code, the default is 0.125
      which is the maximum timestep available.
* Extra
    * ``-l`` Optionally, the lagrange radii can be calculated and printed using
      this option.
    * ``-a`` With this option is possible to print *all* the information of every
      particle, including Mass, Position, Velocity, Forces and Time-step.
    * ``-g`` With this option is possible to set the amount of GPUs that will be
      used.

As we mentioned before, all the allowed option can be displayed on the standard
output using the ``-h`` option.

.. code-block:: bash

    $ ./gravidy -h

    GraviDy:
      -h [ --help ]         Display this message

    Required options:
      -i [ --input ] <filename>       Input data filename
      -t [ --time ] <value> (=1)      Integration time (In N-body units)
      -r [ --resume ] <filename>.info Resume a simulation with an .info file

    Elective options:
      -o [ --output ] <filename> Output data filename
      -s [ --softening ] <value> Softening parameter (default 1e-4)
      -e [ --eta ] <value>       ETA of time-step calculation (default 0.01)
      -p [ --screen ]            Print summary in the screen instead of a file
      -z [ --interval ] <value>        Output time interval (default 0.125)

    Extra options:
      -l [ --lagrange ]         Print information of the Lagrange Radii in every
                                integration time
      -a [ --all ]              Print all the information of N-particles in every
                                integration time
      -g [ --gpu ] <value> (=0) GPUs to use, by default is the maximum available
                                devices (use even numbers)



Initial conditions
===================

``GraviDy`` reads an ASCII file with the information of all the particles,
in the order: *ID*, *Mass*, *Position* and *Velocity*.
The following is an example of the format:

.. code-block:: bash

    id_1   m_1   rx_1 ry_1 rz_1   vx_1 vy_1 vz_1
    id_1   m_2   rx_2 ry_2 rz_2   vx_2 vy_2 vz_2
    id_1   m_3   rx_3 ry_3 rz_3   vx_3 vy_3 vz_3
    ...
    id_1   m_N   rx_N ry_N rz_N   vx_N vy_N vz_N

Output files
==============

Running a simulation without using the ``-p`` option, will generate the following
files:

.. code-block:: bash

    $ ./gravidy-gpu -i ../input/04-nbody-p1024_m1.in -o MyOut -t 1
    [2017-02-01 21:39:41] [INFO] GPUs: 1
    [2017-02-01 21:39:41] [INFO] Splitting 1024 particles in 1 GPUs
    [2017-02-01 21:39:41] [INFO] GPU 0 particles: 1024
    [2017-02-01 21:39:43] [SUCCESS] Finishing...

    $ ls
    MyOut.out.info
    MyOut.out.log
    MyOut.out.snapshot_0000
    MyOut.out.snapshot_0001
    MyOut.out.snapshot_0002
    MyOut.out.snapshot_0003
    MyOut.out.snapshot_0004
    MyOut.out.snapshot_0005
    MyOut.out.snapshot_0006
    MyOut.out.snapshot_0007
    MyOut.out.snapshot_0008
    MyOut.out.snapshot_0009


The ``info`` file
++++++++++++++++++

This file will contain a brief overview of the general aspects of the simulation
at the moment it started:

.. code-block:: bash

    $ cat MyOut.out.info
    # NumberParticles:                             1024
    # Softening:                                 0.0001
    # EtaTimestep:                                 0.01
    # IntegrationTime:                                1
    # PrintScreen:                                    0
    # InputFilename:      ../input/04-nbody-p1024_m1.in
    # OutputFilename:                         MyOut.out
    # SnapshotNumber:                                 9

* ``NumberParticles:`` The number of the particles in the initial condition file.
* ``Softening:`` `\epsilon` parameter for the softened force interaction.

    .. math::

     \boldsymbol{\ddot{r}}_{i} = -G \sum\limits^{N}_{\substack{j=1\\j\neq i}} {m_{j} \over (r^{2}_{ij} + \epsilon^{2})^{3/2} } \boldsymbol{r}_{ij},

* ``EtaTimestep:`` `\eta` parameter for the time step calculation.

    .. math::

     \Delta t_{i} = \sqrt{\eta\frac{|\boldsymbol{a}_{i}||\boldsymbol{a}_{i}^{(2)}|+|\boldsymbol{j}_{i}|^{2}}{|\boldsymbol{j}_{i}||\boldsymbol{a}_{i}^{(3)}|+|\boldsymbol{a}_{i}^{(2)}|^{2}}}

* ``IntegrationTime:`` Integration time specified by the option ``-t``.
* ``PrintScreen:`` If the option ``-p`` was enabled or not.
* ``InputFilename:`` Initial conditions relative path.
* ``OutputFilename:`` Base name of the output files, specified by the option ``-o``.
* ``SnapshotNumber:`` Number of snapshots generated until the integration stopped.

The ``log`` file
++++++++++++++++++

The information on this file will be displayed on the screen if the option ``-p``
is specified, otherwise:

.. code-block:: bash

    $ cat MyOut.out.log
    #    IteTime    Iter    Nsteps              Energy           RelE           CumE     ElapsedTime   GFLOPS
    00     0.000       0         0   -2.5687759360e-01   0.000000e+00   0.000000e+00    1.081110e-01    0.000
    00     0.125     698     30093   -2.5687765551e-01   2.410004e-07   2.410004e-07    4.642270e-01    6.505
    00     0.250    1262     61319   -2.5687762708e-01   1.106466e-07   1.303538e-07    7.479802e-01    7.135
    00     0.375    1897     91571   -2.5687761632e-01   4.192094e-08   8.843281e-08    1.033500e+00    7.340
    00     0.500    2530    121963   -2.5687759445e-01   8.512492e-08   3.307881e-09    1.325059e+00    7.517
    00     0.625    3132    150924   -2.5687760190e-01   2.899267e-08   3.230055e-08    1.588835e+00    7.640
    00     0.750    3725    180446   -2.5687759832e-01   1.394194e-08   1.835861e-08    1.849849e+00    7.733
    00     0.875    4354    212425   -2.5687773276e-01   5.233652e-07   5.417238e-07    2.147411e+00    7.803
    00     1.000    5160    244165   -2.5687767307e-01   2.323583e-07   3.093654e-07    2.489176e+00    7.730

The columns of this file correspond to:

* ``IteTime:`` Current integration time, by default multiple of the maximum timestep allowed.
* ``Iter:`` Amount of iterations, integration steps.
* ``Nsteps:`` Amount of particles that have been updated up to a certain integration time.
* ``Energy:`` Current total energy of the system.
* ``RelE:`` Relative energy error, :math:`\frac{E_{t} - E_{t-1}}{E_{t-1}}`.
* ``CumE:`` Cumulative energy error, :math:`\frac{E_{t} - E_{t,0}}{E_{t,0}}`.
* ``ElapsedTime:`` Wall clock time.
* ``GFLOPS:`` Floating Point Operations Per Second approximation.

The ``snapshot`` files
+++++++++++++++++++++++

Every snapshot correspond to the current information of the system at a certain
time.
The file is generated in ASCII plain text.

.. code-block:: bash

    $ head -n 5 MyOut.out.snapshot_0002
    # Time:0.2500
         1   9.765625e-04   1.142414e+00  -1.065075e+00  -3.423198e-01  -1.796895e-01  -4.357895e-02   2.527831e-01
         2   9.765625e-04   3.938875e-01  -9.265678e-01  -3.237272e-01   7.087970e-02   2.682987e-01  -1.318306e-01
         3   9.765625e-04   1.335213e-01   3.308229e-01  -3.002197e-01  -6.835964e-01   1.240149e-01  -3.313794e-01
         4   9.765625e-04   2.484300e-01   2.496406e-02  -5.006182e-02  -7.771124e-01   4.239778e-01   2.027512e-01
         ...

The first line display the current integration time of the snapshot.
Columns in this file show ``ID``, ``Mass``, ``Rx``, ``Ry``, ``Rz``, ``Vx``, ``Vy`` and ``Vz``
for every particle.
