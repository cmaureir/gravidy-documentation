GraviDy View
-------------

.. figure:: ../_static/gravidyview.gif
   :width: 640px
   :align: center
   :alt: GraviDyView

The most common ways to verify the evolution of an *N-* body system
is checking the conservation of the energy and angular momentum.

Additionally, it is possible to check different phenomena that occur in the system
like for example, the *core-collapse*.

Besides this, when performing large scale simulations or special initial conditions
it is good to have a visualisation tool, that provide a general view of the
evolution of the system.

We found really attractive to be able to see the evolution of a determined cluster
through the time, which is the reason of the development of this graphical tool.

``GravidyView`` is a lightweight and simple OpenGL *N-* body visualisation tool,
written in C/C++.

More information about the project,
and the code is available on `Gitlab`_:

    .. code-block:: bash

        git clone http://gitlab.com/cmaureir/gravidy-view.git

.. _`Gitlab`: http://gitlab.com/cmaureir/gravidy-view.git
