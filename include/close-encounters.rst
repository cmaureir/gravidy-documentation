Binary and Multiple systems treatment
======================================

.. .. tip::
..     Equations within a note
..     :math:`G_{\mu\nu} = 8 \pi G (T_{\mu\nu}  + \rho_\Lambda g_{\mu\nu})`.
..
.. .. note::
..     Equations within a note
..     :math:`G_{\mu\nu} = 8 \pi G (T_{\mu\nu}  + \rho_\Lambda g_{\mu\nu})`.
..
.. .. danger::
..     Equations within a note
..     :math:`G_{\mu\nu} = 8 \pi G (T_{\mu\nu}  + \rho_\Lambda g_{\mu\nu})`.
..
.. .. warning::
..     Equations within a note
..     :math:`G_{\mu\nu} = 8 \pi G (T_{\mu\nu}  + \rho_\Lambda g_{\mu\nu})`.
..
..
.. .. sidebar:: Test
..
..    This section is split in several parts, due to the amount of details
..    that every step needs.



The treatment for binary evolution in our code is based in the integrator
presented in [Konstantinidis2010]_, which use an Hermite 4th order time-symmetric
integrator, which is suitable for the binaries and multiples evolution
inside an N-body system.

Neighbors
---------

Before entering in the routines explanation, we need to obtain a couple of
constant and extra variables of the system to obtain neighbors around a certain
particle and the close encounters between 2 or more particles.

The first variable that we need to define a close encounter distance:

.. math:: R_{cl} = \frac{2 G \bar{m}}{\sigma^{2}},
    :label: r_close

where :math:`G` is the gravitational constant, :math:`\sigma` is the rms velocity
dispersion and :math:`R_{v}` the virial radius of the cluster, given by

.. math:: \sigma^{2} &= \frac{G N \bar{m}}{2 R_{v}}
    :label: sigma

.. math:: R_{v} &= G \frac{N^{2}\bar{m}}{|2U|}
    :label: r_virial

where :math:`U` is the total potential energy.

Every particle will have a radius :math:`R_{h_{i}}`, which will define the distance
of the neighbor sphere around it:

.. math:: R_{h_{i}} = 5 \sqrt{\frac{N}{2}(m_{i} + m_{g})} R_{cl},
    :label: r_neigh

where :math:`m_{i}` is the mass of the particle,
:math:`m_{g}` is the mass of the heaviest particle in the system,
and :math:`R_{cl}` is given by the equation :eq:`r_close`.

Every particle with a distance **lower** than :math:`R_{h_{i}}` will be consider
a neighbor.

.. note::
  The amount of neighbors will depend of every implementation,
  and it is clearly a parameter to test, and see what is the best scenario
  for our implementation)*

Obtaining neighbors
+++++++++++++++++++++

Since every :math:`i` particle will have a neighbor radius associated,
we need to iterate over all the other :math:`j` particles of the system,
that's why we include a conditional inside the force calculation to save
all the neighbors of a :math:`i` particle.

.. code-block:: c++
    :linenos:
    :emphasize-lines: 10-14

    void Hermite4CPU::force_calculation(Predictor pi, Predictor pj, Forces &fi,
                                        int i, int j, double hi)
    {
        double rx = pj.r[0] - pi.r[0];

        // ...

        double r2   = rx*rx + ry*ry + rz*rz;

        if (sqrt(r2) < hi)
        {
            nb_list[i][fi.nb] = j;
            fi.nb++;
        }

        double rinv = 1.0/sqrt(r2);

        // ...
    }

Those lines will fill out two-dimensional array with the ID of all the :math:`j`
particles which are neighbors of the :math:`i` particle, this will help us
to look for close encounters easily.

Close encounters
-----------------

Let consider a particle :math:`i` with a certain amount of neighbors :math:`j`,
we will find a close encounter if the following conditions are satisfied:

.. math::
    :label: r_crit

    R_{ij} \leq R_{crit_{ij}} = \sqrt{\frac{N}{2}(m_{i} + m_{j})} R_{cl},

.. math::
    :label: e_bind

    V_{ij} < 0,

where :math:`R_{ij}` and :math:`V_{ij}` are the distance and velocity
between the particle :math:`i` and :math:`j` respectively,
:math:`m_{i}` and :math:`m_{j}` their masses.

Binary considerations
-----------------------


Binary (Multiple) system creation
++++++++++++++++++++++++++++++++++

Having close encounters between particles will trigger
the creation of binary systems.

First of all we transfer the members to a different separated system
which will be in charge of the binary evolution, and then
we must compensate the general N-body system, since 2 members are leaving
the general integration, that is why a creation of a new particle it is needed.

This new particle (**ghost**, hereon) is created using the information
of the leaving members, using the center of mass for the position, velocity
and acceleration:

.. Missing equations

Since we removed two particles, and now we add a new one, it is heavy computationally
to reallocate the general arrays with the particle information,
which is the reason use the empty spot of the first member
to allocate the **ghost** particle, and for the second one
we fix its mass as zero, in that way, we include this dead-particle in all the
general integration process, but it is not contributing anything to the system.

.. Diagram with ghost creation

Other important things to take care of are the following:

   * Every binary system has a certain :math:`\eta_{b}` which is determined
     at the beginning of the system, following steps:

    * Get the minimum value :math:`|\boldsymbol{a_{i}}|/|\boldsymbol{\dot{a}_{i}}|` for every **i** member.
    * Use the following equation to get :math:`\eta_{b}` satisfying the condition :math:`dt = \Delta_{t, min}/2`

        .. math::

            dt = \eta_{b} min\left(\frac{|\boldsymbol{a_{i}}|}{|\boldsymbol{\dot{a}_{i}}|}\right)

.. note::

    The **ghost** particle will never update its time step, this will always be the
    minimum.


.. Including more particles
.. +++++++++++++++++++++++++
..
.. ...
..
..
.. Binary (Multiple) system termination
.. ++++++++++++++++++++++++++++++++++++++++
..
.. ...


Hermite 4th order Time-symmetric
--------------------------------

.. toctree::
    :maxdepth: 1

    time-symmetric.rst



Changes in the integration scheme
----------------------------------

Compared to the simple Hermite 4th order integration scheme described on
the section :ref:`hermite`, we need to add a couple of subroutines to handle
the subsystems which are created when we have a close encounter between two or more
particles.

The extra routines are highlighted in the following procedure:

.. code-block:: python
    :emphasize-lines: 9-17, 25, 27-29
    :linenos:

    Update_neighbor_radius()
    Initial_forces_calculation()
    Initial_Time_step_calculation()

    while Current_Integration_Time < TOTAL_TIME:
        # Current integration time (selecting the block of time-steps)
        ITIME = next_integration_time()
        nact = find_particles_to_move(ITIME)  # Find the actives particles in this step
        if check_sub_systems()  # Find active subsystems (binaries or multiples)
            Calculate_sub_system_timestep()
            Subsystem_Pos_Vel_Prediction()
            # Since it's an Hermite 4th order Time-symmetric: P(EC)^3
            for i in range(3):
                Subsystem_Calculate_Forces()
                Subsystem_Calculate_Perturbations()
                Subsystem_Pos_Vel_Correction()
            Subsystem_update_time()
        # Save the old forces of the previous step
        save_old_acc_jrk(nact);
        # Now the integration continues with all the particles which are not
        # members of the systems, including the ghost particles.
        Hermite_prediction()
        Forces_update()
        Calculate_new_timesteps()
        nenc = check_for_close_encounters()
        Hermite_correction()
        if nenc > 0:
            create_new_subsystems()
        check_for_subsystems_termination()

.. Those routines are based in the next main ideas.


.. [Konstantinidis2010] MYRIAD: A new N-body code for simulations of Star Clusters
    http://arxiv.org/abs/1006.3326
